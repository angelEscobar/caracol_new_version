# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 12:49:56 2022

@author: angel
"""

import psycopg2
import pandas as pd
import os
import yaml
from pathlib import Path
from datetime import datetime
import argparse
import re
import matplotlib.pyplot as plt
import pytz
import json

mexico= pytz.timezone('America/Mexico_City')

try:
    conn = psycopg2.connect(
        host="192.168.131.16",
        port=5432,
        database="twtfocalevents",
        user="angel",
        password="agl13tlab0371")

except:
    conn = psycopg2.connect(
        host="132.247.170.140",
        port=3716,
        database="twtfocalevents",
        user="angel",
        password="agl13tlab0371")

field_name = "event"

event_names=["8M2022", "queretaroFem", "antifeminismo", "mujeresPorLa4T", "reformaElectrica", "casoLoret", "guacamaya", "narcobloqueos", "revocacion2022", "docscubanos", "hayTiroEleccionesMx2022", "noSequiaSaqueo", "soberaniaEnergetica", "eleccionesColombia", "nuevoleonAgua", "eleccionesMx2022", "periodistas", "jesusErnesto"]


file= os.path.join(
      "Caracol","eventos_tags.txt"
    )

with open(file, 'r') as file:
    eventos_tag = json.load(file)
    
file= os.path.join(
      "Caracol","etiquetas.txt"
    )

with open(file, 'r') as file:
    etiquetas = json.load(file)
    
# count=0
event_hahs_tag_dic=dict()
for event_name in event_names:
    
    print(event_name)
    out_field = ",".join(["author_id", "author_handle"])
    
    # %%============================================== #
    
    table_name = "tweets"
    query = """SELECT {out_field} FROM twitter_data.{table}
    WHERE (event=\'{event}\');
    """.format(out_field = out_field, table=table_name, event=event_name)
    
    # generamos un cursor para interactuar con la base de datos
    cur = conn.cursor()
    cur.execute(query)
    
    tweets = cur.fetchall()
    
    
    out_field = ",".join(["hashtags"])

    # %%============================================== #

    table_name = "tweets"
    query = """SELECT {out_field} FROM twitter_data.{table}
    WHERE (event=\'{event}\');
    """.format(out_field = out_field, table=table_name, event=event_name)
    
    # generamos un cursor para interactuar con la base de datos
    cur = conn.cursor()
    cur.execute(query)
    
    tweets = cur.fetchall()
    
    tweets_hashtags=[]
    list_has=[]
    count=0
    ht_counts = dict()
    for tweet in tweets:
        # print(tweet)
        if tweet[0] !=None:
            for hashtag in tweet[0]:
                ht_mod = hashtag.lower()
                if ht_mod not in ht_counts:
                    ht_counts[ht_mod] = 1
                else:
                    ht_counts[ht_mod] += 1
    
    indice = [range(len(ht_counts))]          
    
    df=pd.Series(ht_counts).to_frame('Frecuencia')  
    df=df.sort_values(by=['Frecuencia'], ascending=False)
    df=df.reset_index()
    df=df[:30].rename(columns={"index": 'Hashtag'})
    
    # for evento in eventos_tag:
    print(eventos_tag[event_name])
    
    hashtags=df.Hashtag.values.tolist()
    
    for hashtag in hashtags:
        if hashtag not in event_hahs_tag_dic:
            event_hahs_tag_dic[hashtag]={
                "evento":event_name,
                "tags":eventos_tag[event_name]["tags"]
                }
        else:
            event_hahs_tag_dic[hashtag]={
                "evento":event_name,
                "tags":eventos_tag[event_name]["tags"]+event_hahs_tag_dic[hashtag]["tags"]
                }

file= os.path.join(
      "Caracol","hashtags_eventos_tags.json"
    )

with open(file, "w") as write_file:
    json.dump(event_hahs_tag_dic, write_file)